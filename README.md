# Print & Post Download Page

### Note 
* **This app requires Java to run, to download Java please visit this [official Java download site](https://java.com/en/).**
* **When Java was already installed, add this link `https://bitbucket.org/` on Java Control Panel exception site list.**

___
If you have Java installed on your PC already and added `https://bitbucket.org/` in the exeption site list, click the button below to start installing the app (this works for both **Windows Vista and up**, and **Mac OS**):

[![PPT Install](https://bitbucket.org/develmode/print-and-post-download/downloads/ppt-install-button.png)](https://bitbucket.org/develmode/print-and-post-download/downloads/print-and-post-1.0.jnlp)







___
#### Temporary Deployment For Mac (Troubleshooting)

[![Old PPT Install on Mac](https://bitbucket.org/develmode/print-and-post-download/downloads/old-ppt-install-button-mac.png)](https://bitbucket.org/develmode/print-and-post-download/downloads/print-and-post-mac.jnlp)
